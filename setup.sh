curdir=`pwd`

root_path=`dirname "$0"`
echo $curdir $root_path
if [ ! $curdir -ef $root_path  ]; then
    echo "ERROR: You must setup under the same directory of this script"
    return 1
fi

config_variable() {
	v=$(python -c "import sys;sys.path.append(\"${root_path}\");import config; print config.$1")
	echo $v
}

root_path=$(config_variable root_path)
MG5_path=$(config_variable MG5_path)
AtlasProject=$(config_variable AtlasProject)
AtlasVersion=$(config_variable AtlasVersion)

echo $root_path

install_lhapdf() {
    MG5_name=$(config_variable MG5_name)
    python -c "import sys;from cStringIO import StringIO;sys.path.append(\"${MG5_path}\");from madgraph.interface.madgraph_interface import MadGraphCmd;mg5=MadGraphCmd();sys.stdin = StringIO(\"N\n\");mg5.do_install(\"lhapdf6\")"
    unset MG5_name
    cd $curdir
}

set_lhapdf() {
	# $MG5_path/HEPTools/HEPToolsInstallers/HEPToolInstaller.py lhapdf6 --mg5_path=$MG5_path --prefix=$MG5_path/HEPTools/
	lhapdf_path=$(python -c "import config; ospath=config.os.path; print ospath.abspath(ospath.join(config.lhapdf_path, ospath.pardir, ospath.pardir))")
    export PATH=$HOME/.local/bin:$lhapdf_path/bin/:$PATH
    export LD_LIBRARY_PATH=`lhapdf-config --libdir`:$LD_LIBRARY_PATH
    export LHAPDF_DATA_PATH=`lhapdf-config --datadir`
    export PYTHONPATH=`lhapdf-config --prefix`/lib/python2.7/site-packages/:$PYTHONPATH
    unset lhapdf_path
}

# Setup Athena
mkdir -p $root_path/Core/AnalysisTop/{build,source} && cd $root_path/Core/AnalysisTop/build
acmSetup $AtlasProject,$AtlasVersion

# Install & Configure LHAPDF6
install_lhapdf
set_lhapdf

# Build 2HDMC
make CalcPhysGamma -C Core/2HDMC-1.7.0

unset set_lhapdf
unset config_variable
unset MG5_path
unset AtlasProject
unset AtlasVersion
unset curdir
unset file_path
