import os
import sys
import shutil
import Process
import subprocess
import glob
import tarfile
from collections import OrderedDict
import utils
import config as c
import Core.EventMatching.Events
from Core.EventMatching.TreeReader import TreeReader as tr
tr.get_logger().level = 20
import madgraph.various.lhe_parser as lhe_parser
import imp
import tempfile
import re
import logging

logger = c.getLogger("2HDM_ttbarSI.Sim")

def get_fct(logger = None):
    if logger is None:
        def fct(idle, run, finish):
            l = "Idle: {:3},  Running: {:3},  Completed: {:3}".format(idle, run, finish)
            print l
    else:
        def fct(idle, run, finish):
            l = "Idle: {:3},  Running: {:3},  Completed: {:3}".format(idle, run, finish)
            logger.info(l)
    return fct

# some helper functions
def get_decay_chain(particle):
    """Get decay chain of a __TruthParticle__
    
    Get all the children of a __TruthParticle__ including itself.
    
    Parameters
    ----------
    particle : {TruthParticle}
        A __TruthParticle__ instance.
    
    Yields
    ------
    children : {TruthParticle}
        The particle itself and all of its children.
    }
    """
    yield particle
    for i in xrange(particle.nChildren()):
        for child in get_decay_chain(particle.child(i)):
            yield child

def category(p, prefix):
    pdgId = p.pdgId()
    anti = "x" if pdgId < 0 else ""
    if p.isW() or p.isZ():
        return prefix + "W"
    if pdgId in (6, -6):
        return "t" + anti
    if (pdgId in (5, -5)) and p.parent().isTop():
        return prefix + "b"
    return prefix + "j" + anti

def category_lhe(p, prefix):
    pdgId = p.pid
    anti = "x" if pdgId < 0 else ""
    if p.pid in (23, 24, -23, -24):
        return prefix + "W"
    if pdgId in (6, -6):
        return "t" + anti
    if (pdgId in (5, -5)) and p.mother1.pid in (6, -6):
        return prefix + "b"
    return prefix + "j" + anti    

def short_name(file_path):
    file_path = os.path.basename(file_path).split(".")
    return ".".join([file_path[3], filter(lambda s: s.startswith("_"), file_path)[0]])

def get_matching_events(flist, ffmt, callables = ["pdfInfo"]):
    ev_gen = (tr(f, ffmt, callables)() for f in flist)
    events = next(ev_gen)
    for _events in ev_gen:
        events.extend(_events)
    return events


def matching(f1list, f2list, ffmt1, ffmt2, callables1 = ["pdfInfo"], callables2 = ["pdfInfo"], yields = [len, lambda matches: [m.eventNumber for m in matches]]):
    t1 = get_matching_events(f1list, ffmt1, callables1)
    t2 = get_matching_events(f2list, ffmt2, callables2)
    try:
        t1.match(t2)
    except RuntimeWarning:
        raise RuntimeError("Some Events find no match or multiple matches. Please check!")
    for ev in t2:
        yield [y(ev.matches) for y in yields]

def check_rc():
    if "ROOTCOREBIN" not in os.environ:
        logger.warn(ImportWarning("Rootcore not yet set. Processing without Rootcore configured. Accessing DAOD files is not possible in this mode."))

if utils.get_mg5version(c.MG5_path) >= (2,6,0):
    def get_me(matrix_module):
        return matrix_module.get_value
    def initialisemodel(matrix_module):
        return matrix_module.initialisemodel
else:
    def get_me(matrix_module):
        def _get_me(*args):
            return matrix_module.get_me(*args)[0]
        return _get_me
    def initialisemodel(matrix_module):
        return matrix_module.initialise


class StrIterator(object):
    def __init__(self, a_str):
        t = type(a_str)
        if t is int:
            s = slice(a_str)
        elif t is str:
            s = slice(*(int(i) if i.strip() else None for i in a_str.lstrip("[").rstrip("]").split(":")))
        self.start, self.stop, self.step = s.start or 0, (s.stop or sys.maxint), s.step or 10000
    def __getitem__(self, y):
        return self._xrange.__getitem__(y)
    def __iter__(self):
        return iter(self._xrange)
    def __len__(self):
        return len(self._xrange)
    def __str__(self):
        return "[{}({}):{}({}):{}]".format(self.start, self.start_index, self.stop, self.stop_index, self.step)
    def __repr__(self):
        return str(self)
    @property
    def start_index(self):
        return self.start // self.step
    @property
    def stop_index(self):
        return (((self.stop - self.start)//self.step) * self.step + self.start) // self.step
    @property
    def _xrange(self):
        return xrange(self.start, self.stop_index * self.step, self.step)
    def piecewise(self):
        for i in range(self.start_index, self.stop_index):
            yield StrIterator("[{start}:{stop}:{step}]".format(start = i * self.step, stop = (i+1) * self.step, step = self.step))
    def get_cmd_repr(self):
        return "[{start}:{stop}:{step}]".format(start = self.start, stop = self.stop, step = self.step)

# Simulation Classes

class BasicSim(object):
    def __init__(self, process, slicer = "[:1000000:10000]", ebeam1 = 6500, ebeam2 = 6500, generator = None, dataset = {}, dataset_ext = None, dirname = "", **kwarg):
        if type(process) in (int, str):
            process = Process.ttbarResonanceProcess.from_run_number(process)
        self.process = process
        self.generator = generator
        self.dirname = dirname
        self.dataset_ext = dataset_ext
        self.slicer = slicer
        self._ebeam1 = ebeam1
        self._ebeam2 = ebeam2
        self.dataset = dataset
        self._run_path_fmt = "{ecm}TeV.{process}"
        self._lhebanner = {}
        self._output_path = None
        self.use_slicer = kwarg.pop("use_slicer", None)
        self.decay = kwarg.get("decay", True)
        self.bkg_removal = kwarg.get('bkg_removal', process.sim_config.get('bkg_removal', True))

        self.use_cluster = kwarg.pop('use_cluster', True)
        import Core.clusters
        self.cluster = Core.clusters.from_name[c.cluster_type if self.use_cluster == True else self.use_cluster](cluster_type = None, cluster_status_update=(600,30), cluster_queue='None')
        self.cluster.max_runtime = 3600
    @property
    def nevents(self):
        return self.slicer.stop - self.slicer.start
    @property
    def split_size(self):
        return self.slicer.step
    @property
    def run_path(self):
        if not hasattr(self, "_run_path"):
            self.run_path = None
        return self._run_path
    @run_path.setter
    def run_path(self, run_path):
        if run_path is not None:
            self._run_path_fmt = run_path
        self._run_path = os.path.realpath(os.path.join(c.run_root, self._run_path_fmt.format(ecm = (self.ebeam1 + self.ebeam2)/1000, process = self.process)))
    @property
    def output_path(self):
        if self._output_path is None:
            self._output_path = os.path.join(self.run_path, self.dirname)
        return self._output_path
    @output_path.setter
    def output_path(self, output_path):
        self._output_path = output_path
    @property
    def ebeam1(self):
        return self._ebeam1
    @ebeam1.setter
    def ebeam1(self, ebeam1):
        self._ebeam1 = ebeam1
        self.run_path = None
    @property
    def ebeam2(self):
        return self._ebeam2
    @ebeam2.setter
    def ebeam2(self, ebeam2):
        self._ebeam2 = ebeam2
        self.run_path = None
    @property
    def work_path(self):
        return os.path.join(self.run_path, "Work", self.dirname)
    @property
    def log_path(self):
        return os.path.join(self.run_path, "LOG", self.dirname)
    def initialise(self):
        pass
    def execute(self):
        pass
    def eventloop_1(self, i):
        pass
    def finalize(self):
        pass
    def validate(self):
        pass
    def check_output(self):
        dic = self.output_path
        if not os.path.exists(dic):
            os.makedirs(dic)
        if not os.path.exists(self.work_path):
            os.makedirs(self.work_path)
        if not os.path.exists(self.log_path):
                    os.makedirs(self.log_path)
    def check_dataset(self, dirname = None, dataset_ext = None, use_slicer = None, inplace = False, use_fname = False, verbose = True):
        use_slicer = use_slicer or self.use_slicer
        dataset_ext = dataset_ext or self.dataset_ext
        dirname = dirname or self.dirname
        dataset = OrderedDict()
        if use_slicer:
            for ipart in xrange(self.slicer.start_index + 1, self.slicer.stop_index + 1):
                for ext in dataset_ext:
                    dname = "{}._{:05d}".format(self.process, ipart)
                    fname = os.path.join(self.run_path, dirname, "{}.{}".format(dname, ext))
                    exists = os.path.exists(fname)
                    logger.info("Looking for {} [{}]".format(os.path.relpath(fname), 'O' if exists else 'X'))
                    if use_fname:
                        dataset.setdefault(fname, exists)
                    else:
                        dataset.setdefault(dname, OrderedDict()).update({ext: os.path.exists(fname)})
        else:
            exists = True
            for ext in dataset_ext:
                pattern = os.path.join(self.run_path, dirname, "{}*.{}*".format(str(self.process), ext))
                logger.info("Search file pattern: {}".format(os.path.relpath(pattern)))
                for fname in sorted(glob.iglob(pattern)):
                    logger.info("Found {} [O]".format(fname))
                    if use_fname:
                        dataset.setdefault(fname, exists)
                    else:
                        dataset.setdefault(os.path.basename(fname).split("."+ext)[0], OrderedDict()).update({ext: exists})
        if inplace:
            self.dataset = dataset
        return dataset
    def remove_dataset(self, dirname = None, dataset_ext = None, use_slicer = None, verbose = True, dry_run = False):
        use_slicer = use_slicer or self.use_slicer
        dataset_ext = dataset_ext or self.dataset_ext
        dirname = dirname or self.dirname
        dataset = [f for f, exists in self.check_dataset(dirname = dirname, dataset_ext = dataset_ext, use_slicer = use_slicer, verbose=False, use_fname = True).iteritems() if exists]
        if verbose or dry_run:
            for d in dataset:
                logger.critical('>>> ' + os.path.relpath(d))
            logger.critical('The samples above are going to be removed. Are you sure? [y/N]')
            if not dry_run:
                if raw_input().strip().lower() == 'y':
                    for d in dataset: os.remove(d)
        else:
            for d in dataset: os.remove(d)
        logger.critical('DONE!')
        

    @property
    def slicer(self):
        return self._slicer
    @slicer.setter
    def slicer(self, a_str):
        if isinstance(a_str, StrIterator):
            self._slicer = a_str
        else:
            self._slicer = StrIterator(a_str)
    def run(self, *arg, **kwarg):
        if self.initialise() == "DONE":
            return
        self.execute()
        self.finalize()
    @property
    def lhebanner(self):
        if not self._lhebanner:
            self._lhebanner = self.get_lhe_eventfile().get_banner()
        return self._lhebanner
    def get_lhe_eventfile(self, num = 0):
        exists = False
        for i, (lhe, exists) in enumerate(self.check_dataset("LHE", dataset_ext = ("tar.gz", "tar", "lhe.gz", "events"), verbose = False, use_fname = True).iteritems()):
            if i == num:
                break
        if exists:
            return lhe_parser.EventFile(lhe)
        else:
            raise IndexError('This event file {} does not exists.'.format(lhe))      
    def gen_xsection(self, mode = 'sum'):
        ret = {}
        if mode == 'sep':
            for l in (l.split() for l in self.lhebanner['init'].splitlines()[1:]):
                if l[0].startswith('<'):
                    break
                ret['P' + l[-1].strip()] = float(l[0])
            ret['total'] = sum(ret.values())
            return ret
        try:
            xsec = next(re.finditer(r"#  Integrated weight \(pb\)  :\s*(\S+)", self.lhebanner['mggenerationinfo'])).group(1)
        except:
            xsec = input("Can't find xsection from %s. Please assign one [pb]: " % os.path.relpath(self.output_path))
        ret['total'] = float(xsec)
        return ret

class PartonLevelSim(BasicSim):
    def __init__(self, process, slicer = "[:1000000:10000]", iseed = 21, dynamical_scale_choice = 3, *arg, **kwarg):
        super(PartonLevelSim, self).__init__(process, slicer = slicer, *arg, **kwarg)
        self.dataset_ext = ("tar.gz", "tar", "lhe.gz", "events")
        self.iseed = iseed
        self.dynamical_scale_choice = dynamical_scale_choice
        self.generator = "MG5"
        if isinstance(self.process, Process.THDMaTtRes):
            self.param_card = kwarg.get("param_card", '/tmp/param_card.dat')
        else:
            self.param_card = kwarg.get("param_card", "")
        self.dirname = "LHE"
        self._systematics = kwarg.setdefault("systematics", [])

    def initialise(self, check_only = False):
        if all(any(d.values()) for d in self.check_dataset(use_slicer = True, verbose = False).values()):
            try:
                self.process.read_param_card(self.lhebanner['slha'])
            except AttributeError:
                pass
            return "DONE"
        elif self.param_card == '/tmp/param_card.dat':
            pass
        if check_only:
            return "NOT DONE"
        self.check_output()
        do_RW = self.process.reweighted_from != None
        self.mg5_output = os.path.join(self.work_path, "PROC_{SIM}_gg{S}ttx{I}".format(SIM = "SIM" if not do_RW else "RW",
                                                                                    S = self.process.mediator,
                                                                                    I = "I" if self.process.signal_type == "SI" else ""))
        if os.path.exists(self.mg5_output):
            RunWeb = os.path.join(self.mg5_output, "RunWeb")
            if os.path.exists(RunWeb):
                os.remove(RunWeb)
            return
        matrixfname = "matrix_madevent_group_v4.inc" if not do_RW else "matrix_standalone_v4.inc"
        matrixf = os.path.join(c.MG5_path, "madgraph", "iolibs", "template_files", matrixfname)
        template_matrixf = os.path.join(c.root_path, "templates", ".".join([matrixfname] + ([] if self.process.signal_type != "SI" or not self.bkg_removal else ["SI"])))
        logger.debug('Replace "{}" with "{}".'.format(os.path.relpath(matrixf), os.path.relpath(template_matrixf)))
        shutil.copyfile(template_matrixf, matrixf)
        mg5_input_file = os.path.join(self.output_path, str(self.process) + ".mg5")
        with open(mg5_input_file, "w") as mg5_input:
            with open(os.path.join(c.root_path, "templates", "MG5_build.mg5"), "r") as mg5_build:
                mg5_input.write(mg5_build.read().format(p = self.process, c = c, sim = self, mg5_output = self.mg5_output, commented = "#" if (do_RW or not self.decay) else "", standalone = "standalone" if do_RW else ""))
        with open(mg5_input_file, "r") as of:
            logger.info(of.read())
        mg5_executable = subprocess.Popen([os.path.join(c.MG5_path, "bin", "mg5_aMC"), mg5_input_file])
        mg5_executable.communicate()
        if mg5_executable.returncode:
            raise OSError("MG5 Error")
        # logger.info(str(mg5_executable.returncode))
        #subprocess.Popen([os.path.join(mg5_output, "bin", "generate_event"), mg5_input_file], stdout = sys.stdout, stdin = subprocess.PIPE).communicate()
    @property
    def systematics(self):
        if self._systematics is True:
            systematics = ["set systematics_arguments ['--pdf=central', '--dyn=-1', '--mur=0.5,1,2', '--muf=0.5,1,2', '--alps=0.5,1,2', '--together=mur,muf,dyn,alps']"]
        elif self._systematics is False:
            systematics = []
        else:
            systematics = self._systematics
        systematics.insert(0, "set use_syst {}".format(bool(self._systematics)))
        return "\n".join(systematics)
    @systematics.setter
    def systematics(self, option):
        self._systematics = option
    def execute(self):
        if self.param_card == '/tmp/param_card.dat':
            self.process.write_param_card(self.param_card)
        mg5_input_file = os.path.join(self.output_path, str(self.process) + ".mg5")
        with open(mg5_input_file, "w") as mg5_input:
            with open(os.path.join(c.root_path, "templates", "MG5_run.mg5"), "r") as mg5_build:
                mg5_input.write(mg5_build.read().format(p = self.process, sim = self, c = c, mg5_output = self.mg5_output))
        with open(mg5_input_file, "r") as of:
            logger.info(of.read())
        mg5_executable = subprocess.Popen([os.path.join(self.mg5_output, "bin", "madevent"), mg5_input_file])
        mg5_executable.communicate()
        # logger.info(str(mg5_executable.returncode))
        event_folder = sorted(glob.iglob(os.path.join(self.mg5_output, 'Events', 'run_*')))[-1]
        lhe = lhe_parser.EventFile(os.path.join(event_folder, 'unweighted_events.lhe.gz'))
        for i, n in enumerate(self.slicer, self.slicer.start_index):
            sub_lhe_name = "{}._{:05d}.events".format(self.process, i+1)
            with open(os.path.join(event_folder, sub_lhe_name), 'w') as sub_lhe:
            #write the banner to the output file
                sub_lhe.write(lhe.banner)
                # Loop over all events
                for en, event in enumerate(lhe):
                    sub_lhe.write(str(event))
                    if en+1 == self.slicer.step:
                        break
                sub_lhe.write('</LesHouchesEvent>\n')
            with tarfile.open(os.path.join(self.output_path, sub_lhe_name.replace('.events', '.' + self.dataset_ext[0])),'w:gz') as lhe_arxiv:
                lhe_arxiv.add(sub_lhe.name, sub_lhe_name, recursive = False)
            os.remove(sub_lhe.name)
    def finalize(self):
        os.chdir(c.root_path)
        try:
            shutil.rmtree(os.path.join(self.work_path))
        except OSError as e:
            logger.error('removing working directory fail!')
            logger.error('Please remove it maullay by doing:\n\trm -rf {}\n or this could cause issues later.'.format(self.work_path))
            raise e
        # subprocess.check_output([os.path.join(self.mg5_output, "bin", "cleanall")])
    def validate(self, use_slicer = False, ignore_error = False, match = "DAOD_TRUTH1", matrix_element = True, matching_strategy = 'safe', **kwarg):
        import ROOT
        if "AOD" in match:
            check_rc()
            utils.rcSetup(os.path.join(c.root_path, "Core", "AnalysisTop"))
        vector = ROOT.vector
        tv = ROOT.TLorentzVector
        if matrix_element:
            ME = BasicRW(self.process, ebeam1 = self.ebeam1, ebeam2 = self.ebeam2)
            ME.output_path = self.output_path
            ME.initialise()
        # Start Tree Header
        header = {"LHEF": ROOT.TNamed("LHEF", ""),
                  match:  ROOT.TNamed(match or "NOMATCH", "")}
        branches = {
                    "weight":         vector(float)(),
                    "t":              vector(   tv)(),
                    # "t_Truth":        vector(   tv)(),
                    "t_W":            vector(   tv)(),
                    # "t_W_Truth":      vector(   tv)(),
                    "t_b":            vector(   tv)(),
                    "t_j":            vector(   tv)(),
                    "t_jx":           vector(   tv)(),
                    "t_hasD":         vector(  int)(),
                    "t_hasS":         vector(  int)(),
                    "t_hasElectron":  vector(  int)(),
                    "t_hasMuon":      vector(  int)(),
                    "t_hasTau":       vector(  int)(),
                    "t_hasB":         vector(  int)(),
                    "tx":             vector(   tv)(),
                    "tx_W":           vector(   tv)(),
                    "tx_b":           vector(   tv)(),
                    "tx_j":           vector(   tv)(),
                    "tx_jx":          vector(   tv)(),
                    "tx_hasD":        vector(  int)(),
                    "tx_hasS":        vector(  int)(),
                    "tx_hasElectron": vector(  int)(),
                    "tx_hasMuon":     vector(  int)(),
                    "tx_hasTau":      vector(  int)(),
                    "tx_hasB":        vector(  int)(),
                    "ttx":            vector(   tv)(),

                    "g1":             vector(   tv)(),
                    "g2":             vector(   tv)(),
                    "ME":             vector(float)(),
                    "aS":             vector(float)(),
                    "ev_hasShowered": vector(  int)(),
                    "eventNumber":    vector(  int)(),
                    }
        for branchname, address in branches.iteritems():
            if "_has" in branchname:
                address.push_back(0)
        # End Tree Header
        en = 0
        for dname, exts in self.check_dataset(inplace = False, use_slicer = use_slicer).iteritems():
            m1list = []
            m2list = []
            skip = False
            for ext, is_exist in exts.iteritems():
                logger.info('Runnig: {}'.format(dname))
                output = os.path.join(self.output_path, dname + "." + ext)
                m1list = [output]
                logger.info('Output path: {}'.format(output))
                if is_exist:

                    ntup = output.replace("." + ext, ".NTUP_LHE.root.tmp")
                    if os.path.exists(ntup.rsplit('.tmp', 1)[0]):
                        skip = True
                    if skip:
                        continue

                    if matching_strategy == 'fast':
                        logger.info('Fast matching mode. Assume one-to-one file correspondence. Only for validation purposes. Do not use this for officially generated samples.')
                        m2list = [os.path.join(self.run_path, match, dname + "." + match[:len(match)-match[-1].isdigit()] + ".root") if match else ""]
                    else:
                        logger.info('Safe matching mode. All events are put into matching at once')
                        pat = os.path.join(self.run_path, match, '*.{}.root'.format(match))
                        logger.info('Searh pattern for files being matched to: "{}"'.format(pat))
                        m2list = list(glob.glob(os.path.join(self.run_path, match, '*.{}.root'.format(match[:-1] if 'DAOD_TRUTH' in match else match)))) if match else ['']
                    logger.debug('Matching')
                    logger.debug('\tData({output})'.format(output = output))
                    logger.debug('to')
                    for m2 in m2list:
                        logger.debug('\tData({})'.format(m2))
                    break

            if skip:
                logger.info('Output exists. SKIP!')
                continue
            if not (is_exist or ignore_error):
                raise OSError("Data({output}) does not exist. Please check!".format(output = output))

            if match and (any(not os.path.exists(m2) for m2 in m2list)):
                raise OSError("Some of the following samples do not exist. Please check!\n{}".format('\n'.join("\tData({})".format(m2) for m2 in m2list)))
            elif match:
                Core.EventMatching.Events.logger.setLevel(logging.INFO)
                get_matched = matching(m2list, m1list, "AOD" if "AOD" in match else match, "LHE")

            ntup = ROOT.TFile(ntup, "RECREATE")
            new_tree = ROOT.TTree("CollectionTree", "CollectionTree")
            for branchname, address in branches.iteritems():
                new_tree.Branch(branchname, address)
            header["LHEF"].SetTitle(output)
            header[match].SetTitle(m2list[0])
            lhe_file = lhe_parser.EventFile(output)
            if matrix_element:
                ME.set_param_card(lhe_file.get_banner()['slha'])

            for en, ev in enumerate(lhe_file, en):
                if en % 5000 == 0:
                    logger.info("#{}".format(en) )
                branches["weight"].push_back(float(ev.wgt))
                for wgt_id, wgt in sorted(ev.parse_reweight().iteritems(), key = lambda item: int(item[0])):
                    branches["weight"].push_back(wgt)
                branches["aS"].push_back(float(ev.aqcd))
                gnum = 0
                for top in ev:
                    if top.pid == 21:
                        gnum += 1
                        branches["g" + str(gnum)].push_back(ROOT.TLorentzVector(top.px, top.py, top.pz, top.E))
                    if top.pid in (-6, 6):
                        name = "t_" if top.pid == 6 else "tx_"
                        for i, p in enumerate(ev.get_decay(top.pid)):
                            assert i <= 5, "More than t -> W (W -> j j) b. Please check."
                            branches[category_lhe(p, name)].push_back(ROOT.TLorentzVector(p.px, p.py, p.pz, p.E))
                            for pid, lep in zip((11, 13, 15, 1, 3, 5), ("Electron", "Muon", "Tau", "D", "S", "B")):
                                if abs(p.pid) == pid:
                                    branches[name + "has" + lep][0] += 1
                                    break
                branches["ttx"].push_back(branches["t"][0] + branches["tx"][0])
                # return branches, ME
                branches["ME"].push_back(ME._execute(branches) if matrix_element else 0.)
                if match:
                    is_matched, eventNumbers = next(get_matched)
                    branches["ev_hasShowered"][0] =  is_matched
                    for eventNumber in eventNumbers:
                        branches["eventNumber"].push_back(eventNumber)
                new_tree.Fill()
                for branchname, address in branches.iteritems():
                    if ("_has" in branchname) or ("chi2" in branchname):
                        address[0] = 0
                        continue
                    address.clear()
            ntup.cd()
            for name in header.values():
                name.Write()
            ntup.Write()
            ntup.Close()
            shutil.move(ntup.GetName(), ntup.GetName().rsplit('.tmp', 1)[0])
            lhe_file.close()
        if matrix_element:
            ME.finalize()

class TruthLevelSim(BasicSim):
    def __init__(self, process, slicer = "[:1000000:5000]", *arg, **kwarg):
        super(TruthLevelSim, self).__init__(process, slicer = slicer, *arg, **kwarg)
        self.dataset_ext = ("DAOD_TRUTH.root",)
        self.generator = "MadGraph5+Pythia6"
        self.dirname = c.TRUTH_FMT
        self.evgenJobOpts = kwarg.get("evgenJobOpts", None)
    def initialise(self):
        self.check_output()
        self.evnt_dir = os.path.join(self.output_path, '..', "EVNT")
        if not os.path.exists(self.evnt_dir):
            os.makedirs(self.evnt_dir)
        self.athena = os.path.join(c.root_path, "Core", "Athena")
    def execute(self):
        outputs = map(short_name, self.check_dataset(inplace = False).iterkeys())
        firstEvent = 0
        for dataset, exts in self.check_dataset(dirname = "LHE", dataset_ext = ["tar.gz"], inplace = False).iteritems():
            for ext, is_exist in exts.iteritems():
                if is_exist:
                    inputGeneratorFile = os.path.join(self.output_path, '..', "LHE", ".".join([dataset, ext]))
            short = short_name(inputGeneratorFile)
            dsid, ipart = short.split("._")
            dsid = dsid[3:]
            ipart = ipart[len(ipart) - 3:]
            if short in outputs: 
                firstEvent += 5000
                continue
            subwork_path = os.path.join(self.work_path, short)
            try:
                os.makedirs(subwork_path)
            except OSError:
                pass
            outputEVNTFile = os.path.join(self.evnt_dir, os.path.basename(inputGeneratorFile.split(".tar")[0]) + ".EVNT.root")
            with open(os.path.join(c.root_path, "templates", "Athena_run.sh"), "r") as Athena_run_template:
                Athena_run_template = Athena_run_template.read().format(root_path = c.root_path,
                                                                        work_path = subwork_path,
                                                                        output_path = self.output_path,
                                                                        process = self.process,
                                                                        run_number = self.process.dataset_number if str(self.process.dataset_number).isdigit() else "999999",
                                                                        jo = self.find_jo(inputGeneratorFile, evgenJobOpts = self.evgenJobOpts),
                                                                        outputEVNTFile = outputEVNTFile,
                                                                        inputGeneratorFile = inputGeneratorFile,
                                                                        outputFile = os.path.join(self.output_path, os.path.basename(inputGeneratorFile.split(".tar")[0]) + ".{}".format(self.dataset_ext[0])),
                                                                        fmt = self.dirname.split('_'),
                                                                        evgenJobOpts = ("evgenJobOpts=" + self.evgenJobOpts) if self.evgenJobOpts else "",
                                                                        firstEvent = firstEvent,
                                                                        Athena = self.athena,
                                                                        ecmEnergy = self.ebeam1 + self.ebeam2)
                with open(os.path.join(subwork_path, "p{}.{}".format(dsid, ipart)), "w") as Athena_run:
                    logger.info(Athena_run_template)
                    Athena_run.write(Athena_run_template)
            if self.use_cluster:
                subprocess.check_call(['chmod', '+x', Athena_run.name])
                job_id = self.cluster.get_identifier()
                self.cluster.submit(Athena_run.name,
                                       stdout = os.path.join(self.log_path, 'out.%s' % job_id),
                                       stderr = os.path.join(self.log_path, 'out.%s' % job_id),
                                       log    = os.path.join(self.log_path, 'log.%s' % job_id),
                                       # requirement = 'Requirements = ( OpSysAndVer == "SL6" )'
                                       )
            else:
                subprocess.check_output(['sh ' + Athena_run.name], shell = True, executable="/bin/zsh")
        if self.use_cluster:
            self.cluster.wait(None, fct = get_fct(logger = logger))
    def find_jo(self, inputGeneratorFile, evgenJobOpts = None):
        evgenJobOpts = evgenJobOpts or self.evgenJobOpts
        run_number = os.path.split(inputGeneratorFile)[1].split(".")[3]
        jo = glob.glob(os.path.join(c.root_path, "TestArea-MC12JobOptions-00-16-76", "MC12JobOptions-00-16-76", "share", "DSID{:x<6.3}".format(run_number) , "*." + run_number + ".*.py"))
        if jo:
            if evgenJobOpts:
                return os.path.join("MC12JobOptions", os.path.split(jo[0])[1])
            else:
                return jo[0]
        else:
            return os.path.join(self.athena, "jo.py")
    def validate(self, use_slicer = False, ignore_error = False, new_weights = None, algorithms = ['SL'], **kwargs):
        check_rc()
        import ROOT
        vector = ROOT.vector
        tv = ROOT.TLorentzVector
        if (new_weights == None) and (self.process.reweighted_from != None):
            rw = TruthLevelRW(self.process, slicer = self.slicer, ebeam1 = self.ebeam1, ebeam2 = self.ebeam2)
            rw.initialise()
            new_weights = rw.execute(use_slicer = use_slicer)
            PreSim = rw.PreSim
        else:
            PreSim = self
        if "DAOD" in self.dataset_ext[0]:
            utils.rcSetup(os.path.join(c.root_path, "Core", "AnalysisTop"))
        # Start Tree Header
        branches = {
                    "weight":         vector(float)(),
                    "t":              vector(   tv)(),
                    "t_W":            vector(   tv)(),
                    "t_b":            vector(   tv)(),
                    "t_j":            vector(   tv)(),
                    "t_jx":           vector(   tv)(),
                    "t_hasD":         vector(  int)(),
                    "t_hasS":         vector(  int)(),
                    "t_hasElectron":  vector(  int)(),
                    "t_hasMuon":      vector(  int)(),
                    "t_hasTau":       vector(  int)(),
                    "t_hasB":         vector(  int)(),
                    "tx":             vector(   tv)(),
                    "tx_W":           vector(   tv)(),
                    "tx_b":           vector(   tv)(),
                    "tx_j":           vector(   tv)(),
                    "tx_jx":          vector(   tv)(),
                    "tx_hasD":        vector(  int)(),
                    "tx_hasS":        vector(  int)(),
                    "tx_hasElectron": vector(  int)(),
                    "tx_hasMuon":     vector(  int)(),
                    "tx_hasTau":      vector(  int)(),
                    "tx_hasB":        vector(  int)(),
                    "ttx":            vector(   tv)(),
                    "ev_hasRec":      vector(  int)()
                    }
        for branchname, address in branches.iteritems():
            if "_has" in branchname:
                address.push_back(0)
        if all(self.check_dataset(dataset_ext = ['NTUP_TRUTH.root'], use_fname = True, use_slicer = False).values() or (False, )):
            logger.info("DONE")
            return
        ntup = os.path.join(self.output_path, self.process.__str__(True) + ".NTUP_TRUTH.root") # To do: split files into small pieces
        ntup = ROOT.TFile(ntup, "RECREATE")
        new_tree = ROOT.TTree("CollectionTree", "CollectionTree")
        Algorithm = __import__("Algorithm")
        algorithms = [getattr(Algorithm, alg)() for alg in algorithms]
        for alg in algorithms:
            branches.update(alg.branches)
        for branchname, address in branches.iteritems():
            new_tree.Branch(branchname, address)
        # End Tree Header
        chain = ROOT.TChain("CollectionTree")
        for output, exts in PreSim.check_dataset(dirname = self.dirname, dataset_ext = self.dataset_ext, inplace = False, use_slicer = use_slicer).iteritems():
            for ext, is_exist in exts.iteritems():
                output = os.path.join(PreSim.run_path, self.dirname, output + "." + ext)
                if is_exist:
                    break
            if not (is_exist or ignore_error):
                raise OSError("Data({output}) does not exist. Please check!".format(output = output))
            chain.Add(output)
        tree = ROOT.xAOD.MakeTransientTree(chain)
        for alg in algorithms:
            alg.set_tree(tree)
        for en in xrange(tree.GetEntriesFast()):
            if en % 5000 == 0:
                logger.info("#{}".format(en) )
            tree.GetEntry(en)
            branches["weight"].push_back(tree.EventInfo.mcEventWeight(0) * (next(new_weights) if new_weights else 1.))
            for top in tree.TruthParticles:
                if top.isTop():
                    name = "t_" if top.pdgId() == 6 else "tx_"
                    for i, p in enumerate(get_decay_chain(top)):
                        assert i <= 5, "More than t -> W (W -> j j) b. Please check."
                        branches[category(p, name)].push_back(p.p4()*0.001)
                        for pid, lep in zip((11, 13, 15, 1, 3, 5), ("Electron", "Muon", "Tau", "D", "S", "B")):
                            if abs(p.pdgId()) == pid:
                                branches[name + "has" + lep][0] += 1
                                break
            branches["ttx"].push_back(branches["t"][0] + branches["tx"][0])

            # selections
            for alg in algorithms:
                branches['ev_hasRec'][0] = alg()

            new_tree.Fill()
            for branchname, address in branches.iteritems():
                if ("_has" in branchname) or ("chi2" in branchname):
                    address[0] = 0
                    continue
                address.clear()
        ntup.Write()
        ntup.Close()
        if (new_weights == None) and (self.process.reweighted_from != None):
            rw.finalize()
        ROOT.xAOD.ClearTransientTrees()
        chain.GetFile().Close()

class BasicRW(PartonLevelSim):
    """Basic RWGT Tools
    
    These classes is only used to compute Matrix Elements, which means it behaves totally different from a __BasicSim__ instance.
    """
    def __init__(self, process, param_card = None, *arg, **kwarg):
        super(BasicRW, self).__init__(process, *arg, **kwarg)
        self.param_card = param_card
        self.matrices = []
        self.PreSim = PartonLevelSim(self.process.reweighted_from or self.process, ebeam1 = self.ebeam1, ebeam2 = self.ebeam2, slicer = self.slicer)
        self.dataset_ext = ["NTUP_LHE.root"]
        self.generator = "RWGT from {p.mass}GeV {p.mediator} tan#beta={p.tanb}".format(p = self.process.reweighted_from or self.process)
        self.me_args = (0,)
        self.decay = False
    def _initialise(self):
        global ROOT
        import ROOT
        self.check_output()
        self.mg5_output = os.path.join(self.work_path, "PROC_{SIM}_gg{S}ttx{I}".format(SIM = "RW",
                                                                                       S = self.process.mediator,
                                                                                       I = "I" if self.process.signal_type == "SI" else ""))
        if '[QCD]' in self.process.sim_config["coupling_order"]:
            self.process.sim_config["coupling_order"] = self.sim_config["coupling_order"].replace('[QCD]','[virt=QCD]')
            self.me_args = (91.2, -1)
        if os.path.exists(self.mg5_output):
            RunWeb = os.path.join(self.mg5_output, "RunWeb")
            if os.path.exists(RunWeb):
                os.remove(RunWeb)
            return

        matrixfname = "matrix_standalone_v4.inc"
        matrixf = os.path.join(c.MG5_path, "madgraph", "iolibs", "template_files", matrixfname)
        template_matrixf = os.path.join(c.root_path, "templates", ".".join([matrixfname] + ([] if self.process.signal_type != "SI" or not self.bkg_removal else ["SI"])))
        logger.debug('Replace "{}" with "{}".'.format(os.path.relpath(matrixf), os.path.relpath(template_matrixf)))
        shutil.copyfile(template_matrixf, matrixf)
        mg5_input_file = os.path.join(self.output_path, str(self.process) + ".mg5")
        with open(mg5_input_file, "w") as mg5_input:
            with open(os.path.join(c.root_path, "templates", "MG5_build.mg5"), "r") as mg5_build:
                mg5_input.write(mg5_build.read().format(p = self.process, sim = self, mg5_output = self.mg5_output, commented = "" if self.decay else "#", standalone = "standalone"))
        with open(mg5_input_file, "r") as of:
            logger.info(of.read())
        mg5_executable = subprocess.Popen([os.path.join(c.MG5_path, "bin", "mg5_aMC"), mg5_input_file])
        mg5_executable.communicate()
        # logger.info(str(mg5_executable.returncode))
    def initialise(self):
        self._initialise()
        for subProcess in glob.iglob(os.path.join(self.mg5_output, "SubProcesses", "P*_*")):
            temp = next(glob.iglob(os.path.join(subProcess, "matrix*py.so")), None)
            if not temp:
                temp = next(tempfile._get_candidate_names())
                try:
                    subprocess.check_output(["make", "-C", subProcess, "SHELL=" + c.shell, "matrix%spy.so" % temp, "-e", "MENUM=%s" % temp])
                except subprocess.CalledProcessError as e:
                    logger.error('Compilation of the ME calculators fail! This usually happens when the working directory was not completely deleted in the previous run.')
                    raise e
                temp = "matrix%spy" % temp
            else:
                temp = os.path.splitext(os.path.basename(temp))[0]
            m = imp.load_module(temp , *imp.find_module(temp, [subProcess]))
            self.matrices.append(m)
        # os.chdir(subProcess)
    def set_param_card(self, param_card = None):
        if param_card is not None:
            if os.path.exists(param_card):
                param_card = os.path.abspath(param_card)
                with open(param_card) as rf:
                    param_card = rf.read()
        self.param_card = param_card
        # why I cd to subprocess directory? I don't remember it at all. It seems relate to loop-induced process.
        os.chdir(next(glob.iglob(os.path.join(self.mg5_output, "SubProcesses", "P*_*"))))

        with tempfile.NamedTemporaryFile(dir = os.path.join(self.mg5_output, "Cards"), suffix = ".dat") as param_card:
            if not self.param_card:
                self.process.write_param_card(param_card.name)
            else:
                param_card.write(self.param_card)
            param_card.seek(0)
            for m in self.matrices:
                initialisemodel(m)(param_card.name)
            self.param_card = param_card.read()
    def _execute(self, ev, slicer = slice(None)):
        return sum(get_me(m)([[ev["g1"][0].E(), ev["g2"][0].E() , ev["t"][0].E() , ev["tx"][0].E() ],
                             [ev["g1"][0].Px(), ev["g2"][0].Px(), ev["t"][0].Px(), ev["tx"][0].Px()],
                             [ev["g1"][0].Py(), ev["g2"][0].Py(), ev["t"][0].Py(), ev["tx"][0].Py()],
                             [ev["g1"][0].Pz(), ev["g2"][0].Pz(), ev["t"][0].Pz(), ev["tx"][0].Pz()]
                             ],
                            ev["aS"][0], *self.me_args) for m in self.matrices[slicer])


class TruthLevelRW(BasicRW):
    def __init__(self, process, slicer = "[:1000000:5000]", param_card = None, *arg, **kwarg):
        super(TruthLevelRW, self).__init__(process, param_card, slicer = slicer, *arg, **kwarg)
        self.dataset_ext = ("NTUP_TRUTH.root",)
        self.slicer = slicer
        self.dirname = c.TRUTH_FMT
    def initialise(self):
        super(TruthLevelRW, self).initialise()
        self.set_param_card()
    def set_param_card(self, param_card = None):
        if not (param_card or self.param_card):
            with tempfile.NamedTemporaryFile(dir = os.path.join(self.mg5_output, "Cards")) as param_card:
                self.process.write_param_card(param_card.name)
                param_card.seek(0)
                for m in self.matrices:
                    initialisemodel(m)(param_card.name)
                self.param_card = param_card.read()
    def execute(self, use_slicer = False):
        for fname, is_exist in self.PreSim.check_dataset(dirname = "LHE", dataset_ext = ("NTUP_LHE.root",), use_slicer = use_slicer, use_fname = True).iteritems():
            if is_exist:
                f = ROOT.TFile(fname)
                ev = f.CollectionTree
                for en in xrange(ev.GetEntries()):
                    ev.GetEntry(en)
                    if ev.ev_hasShowered[0]:
                        yield sum(get_me(m)([[ev.g1[0].E(),  ev.g2[0].E(),  ev.t[0].E(),  ev.tx[0].E()],
                                            [ev.g1[0].Px(), ev.g2[0].Px(), ev.t[0].Px(), ev.tx[0].Px()],
                                            [ev.g1[0].Py(), ev.g2[0].Py(), ev.t[0].Py(), ev.tx[0].Py()],
                                            [ev.g1[0].Pz(), ev.g2[0].Pz(), ev.t[0].Pz(), ev.tx[0].Pz()]
                                           ],
                                           ev.aS[0],
                                           0) for m in self.matrices) / ev.ME[0]

class RecoLevelRW_NTUPCOMMON(TruthLevelRW):
    def __init__(self, process, slicer = "[:1000000:5000]", param_card = None, *arg, **kwarg):
        raise NotImplementedError()
        super(TruthLevelRW, self).__init__(process, param_card, slicer = slicer, *arg, **kwarg)
        self.dataset_ext = ("RW.dat",)
        self.slicer = slicer
        self.dirname = "NTUP_COMMON"

class PsuedoEvent(dict):
    def __init__(self):
        self["g1"] = [ROOT.TLorentzVector(0.0, 0.0, 551.56712469, 551.56712469)]
        self["g2"] = [ROOT.TLorentzVector(0.0, 0.0, -201.38120096, 201.38120096)]
        self["t"] = [ROOT.TLorentzVector(-60.695969943, -272.62047144, 106.53953936, 345.12694882)]
        self["tx"] = [ROOT.TLorentzVector(60.695969943, 272.62047144, 243.64638437, 407.82137683)]
        self["aS"] = [0.0987657830119133]
    def madgraph_style(ev):
        return [[ev["g1"][0].E() , ev["g2"][0].E() , ev["t"][0].E() , ev["tx"][0].E() ],
                [ev["g1"][0].Px(), ev["g2"][0].Px(), ev["t"][0].Px(), ev["tx"][0].Px()],
                [ev["g1"][0].Py(), ev["g2"][0].Py(), ev["t"][0].Py(), ev["tx"][0].Py()],
                [ev["g1"][0].Pz(), ev["g2"][0].Pz(), ev["t"][0].Pz(), ev["tx"][0].Pz()]
               ]
