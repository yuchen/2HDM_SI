#include "THDM.h"
#include "Constraints.h"
#include "DecayTable.h"
#include <iostream>

using namespace std;

int main(int argc, char* argv[]) {

  if (argc < 3) {
    cout << "Usage: ./CalcPhys m tan_beta mt(default: 172.5)\n";
    return -1;
  }

 
  double m_in = (double)atof(argv[1]);
  double tb_in = (double)atof(argv[2]);
  double mt_in;
  if (argc == 4) {
    mt_in = (double)atof(argv[3]);
  }
  else {
    mt_in = 172.5;
  }
  
  double mh_in = 125;
  double mH_in = m_in;
  double mA_in = m_in;
  double mp_in = m_in;
  double sba_in = 1.;
  double l6_in = 0;
  double l7_in = 0;
  int yt_in = 2;
  double m12_2_in = pow(mA_in,2) * tb_in/(1+pow(tb_in,2)); 

  THDM model;

  SM sm; 
  sm.set_qmass_pole(6, mt_in);  
  sm.set_qmass_pole(5, 4.72);
  sm.set_qmass_pole(4, 1.57);
  sm.set_lmass_pole(3, 1.77684);
  sm.set_alpha_s(0.119);
  sm.set_MZ(91.15349);
  sm.set_MW(80.36951);
  sm.set_gamma_Z(2.49581);
  sm.set_gamma_W(2.08856);
  sm.set_GF(1.16637E-5);
  sm.set_alpha(1./137.0359997);
  model.set_SM(sm);


  bool pset = model.set_param_phys(mh_in,mH_in,mA_in,mp_in,sba_in,l6_in,l7_in,m12_2_in,tb_in);

  if (!pset) {
	cerr << "The parameters you have specified were not valid\n";
	return -1;
  }

  model.set_yukawas_type(yt_in);

  // Print Higgs decays to the screen
  DecayTable table(model);
  table.print_decays(2);
  table.print_decays(3);
  cout << "w_H: " << table.get_gammatot_h(2) << endl;
  cout << "w_A: " << table.get_gammatot_h(3) << endl;
}
