import ROOT
from math import sqrt, log10
vector = ROOT.vector
tv = ROOT.TLorentzVector

MW = 83700.
err_MW = 8400.
MthW = 91200.
err_MthW = 11100.
Mtl = 167600.
err_Mtl = 21800.
pTdiff = -1.2
err_pTdiff = 34700.

def permutations(iterable, r=None, with_rest = False):
    # permutations('ABCD', 2) --> AB AC AD BA BC BD CA CB CD DA DB DC
    # permutations(range(3)) --> 012 021 102 120 201 210
    pool = tuple(iterable)
    n = len(pool)
    r = n if r is None else r
    if r > n:
        return
    indices = range(n)
    cycles = range(n, n-r, -1)
    if with_rest:
        yield tuple(pool[i] for i in indices[:r]), tuple(pool[i] for i in indices[r:])
    else:
        yield tuple(pool[i] for i in indices[:r])
    while n:
        for i in reversed(range(r)):
            cycles[i] -= 1
            if cycles[i] == 0:
                indices[i:] = indices[i+1:] + indices[i:i+1]
                cycles[i] = n - i
            else:
                j = cycles[i]
                indices[i], indices[-j] = indices[-j], indices[i]
                if with_rest:
                    yield tuple(pool[i] for i in indices[:r]), tuple(pool[i] for i in indices[r:])
                else:
                    yield tuple(pool[i] for i in indices[:r])
                break
        else:
            return

def nu_rec(nu, l):
    x1, y1 = nu.X(), nu.Y()
    x2, y2, z2, t2 = l.px(), l.py(), l.pz(), l.e()
    c = 2.*(-4*t2**2 + 4*z2**2)
    a = -4*t2**2 + 4*z2**2
    b = 4*MW**2*z2 - 4*t2**2*z2 + 8*x1*x2*z2 + 4*x2**2*z2 + 8*y1*y2*z2 + 4*y2**2*z2 + 4*z2**3
    c = MW**4 + t2**4 +\
        (2*x1*x2 + x2**2 + 2*y1*y2 + y2**2 + z2**2)**2 +\
         2*MW**2*( -t2**2 + 2*x1*x2 + x2**2 + 2*y1*y2 +   y2**2 + z2**2) - \
         2*t2**2*(2*x1**2 + 2*x1*x2 + x2**2 + 2*y1**2 + 2*y1*y2 + y2**2 + z2**2)
    delta = b**2 - 4*a*c
    if delta >= 0.:
        nu.SetXYZM(x1, y1, min((-b + sqrt(delta))/2/a, (-b - sqrt(delta))/2/a, key = abs), 0)
    else:
        return 1

def log10chi2(l, nu, b1, b2, j1, j2):
    chi2 = 0.
    jj = j1 + j2
    jjb = jj + b1
    lnu = l + nu
    lnub = lnu + b2
    chi2 += pow((jj.M()               - MW)     / err_MW,     2)
    chi2 += pow((jjb.M() - jj.M()     - MthW)   / err_MthW,   2)
    chi2 += pow((lnub.M()             - Mtl)    / err_Mtl,    2)
    chi2 += pow((jjb.Pt() - lnub.Pt() - pTdiff) / err_pTdiff, 2)
    return log10(chi2)

class Algorithm(object):
    def __init__(self, tree = None):
        self.set_tree(tree)
    def __call__(self):
        raise NotImplementedError
    def set_tree(self, tree):
        self.tree = tree

class SL(Algorithm):
    def __init__(self, tree = None):
        super(SL, self).__init__(tree)
        self.tv = tv()
        # Cuts
        self.cut_e_pt = 25000.
        self.cut_e_eta1 = 1.37
        self.cut_e_eta2 = 1.52
        self.cut_e_eta3 = 2.47

        self.cut_mu_pt = 25000.
        self.cut_mu_eta = 2.5
        self.cut_nu_pt = 25000.

        self.cut_met = 25000.

        self.cut_jet_pt = 25000.
        self.cut_jet_eta = 2.5

        self.cut_log10chi2 = 0.9

        self.KINEMATIC = 1
        self.BTAGGING = 2
        self.NUREC = 3
        self.CHI2 = 4
        self.ALLPASSED = 0
        self.STATUS_CODE = {
                            self.KINEMATIC: 'Kinematic',
                            self.BTAGGING: '\geq 1 b-labelled',
                            self.NUREC: '\nu Reconstruction',
                            self.CHI2: '\log_{{10}}{{\chi^2}} \le {self.cut_log10chi2}'.format(self = self),
                            self.ALLPASSED: ''
        }

        self.branches = {
                         "truth.t":              vector(   tv)(),
                         "truth.t_W":            vector(   tv)(),
                         "truth.t_b":            vector(   tv)(),
                         "truth.t_j":            vector(   tv)(),
                         "truth.t_jx":           vector(   tv)(),
                         "truth.t_hasElectron":  vector(  int)(),
                         "truth.t_hasMuon":      vector(  int)(),
                         "truth.t_hasTau":       vector(  int)(),
                         "truth.t_hasB":         vector(  int)(),
                         "truth.tx":             vector(   tv)(),
                         "truth.tx_W":           vector(   tv)(),
                         "truth.tx_b":           vector(   tv)(),
                         "truth.tx_j":           vector(   tv)(),
                         "truth.tx_jx":          vector(   tv)(),
                         "truth.tx_hasElectron": vector(  int)(),
                         "truth.tx_hasMuon":     vector(  int)(),
                         "truth.tx_hasTau":      vector(  int)(),
                         "truth.tx_hasB":        vector(  int)(),
                         "truth.ttx":            vector(   tv)(),
                         "truth.chi2":           vector(float)()
                        }
        for branchname, address in self.branches.iteritems():
            if ("_has" in branchname) or ('chi2' in branchname):
                address.push_back(0)
    def __call__(self):
        # Initialization
        t = self.tree
        br = self.branches

        e_pass = []
        mu_pass = []
        j_pass = []
        bj_pass = []

        # Kinematic Cuts
        for p in t.TruthElectrons:
            if p.pt() < self.cut_e_pt:
                continue
            eta = abs(p.eta())
            if (self.cut_e_eta1 < eta < self.cut_e_eta2) or (eta > self.cut_e_eta3):
                continue
            e_pass.append(p)
        for p in t.TruthMuons:
            if p.pt() < self.cut_mu_pt:
                continue
            eta = abs(p.eta())
            if eta > self.cut_mu_eta:
                continue
            mu_pass.append(p)
        l_pass = e_pass + mu_pass

        if len(l_pass) != 1:
            return self.KINEMATIC

        for j in t.AntiKt4TruthWZJets:
            if j.pt() < self.cut_jet_pt:
                continue
            j_pass.append(j)
        if len(j_pass) < 4:
            return self.KINEMATIC

        mpx, mpy, mpz = 0., 0., 0
        for met in t.MET_Truth:
            if met.name() in ('Int', 'IntMuons'):
                mpx += met.mpx()
                mpy += met.mpy()
        met = self.tv
        met.SetXYZM(mpx, mpy, mpz, 0)
        if met.Pt() < self.cut_met:
            return self.KINEMATIC
        # B-Tagging
        for j in j_pass:
            if j.auxdata("PartonTruthLabelID") == 5:
                bj_pass.append(j)
        nbj = len(bj_pass)
        if len(bj_pass) < 1:
            return self.BTAGGING
        for j in bj_pass:
            j_pass.remove(j)
        # Reconstruct Neutrinos
        if nu_rec(met, l_pass[0]):
            return self.NUREC

        # Chi2
        min_chi2_b1b2j1j2 = (float('inf'), tuple())
        l = l_pass[0].p4()
        nu = met
        for bjets, mistagged_bjets in permutations(bj_pass, min(2, nbj), with_rest = True):
            for jets in permutations(mistagged_bjets + tuple(j_pass), 4 - len(bjets)):
                b1, b2, j1, j2 = (bjets+jets)[:4]
                chi2 = log10chi2(l, nu, b1.p4(), b2.p4(), j1.p4(), j2.p4())
                min_chi2_b1b2j1j2 = min(min_chi2_b1b2j1j2, (chi2, (b1, b2, j1, j2)))

        # Store Event
        b1, b2, j1, j2 = min_chi2_b1b2j1j2[1]
        # t is hadronic top
        br['truth.t_b'].push_back(b1.p4()*0.001)
        br['truth.t_j'].push_back(j1.p4()*0.001)
        br['truth.t_jx'].push_back(j2.p4()*0.001)
        br['truth.t_W'].push_back(br['truth.t_j'][-1] + br['truth.t_jx'][-1])
        br['truth.t'].push_back(br['truth.t_W'][-1] + br['truth.t_b'][-1])
        br['truth.t_hasB'][0] = int(b1.auxdata("PartonTruthLabelID") == 5)
        # tx is leptonic top
        br['truth.tx_b'].push_back(b2.p4()*0.001)
        br['truth.tx_j'].push_back(l*0.001)
        br['truth.tx_jx'].push_back(nu*0.001)
        br['truth.tx_W'].push_back(br['truth.tx_j'][-1] + br['truth.tx_jx'][-1])
        br['truth.tx'].push_back(br['truth.tx_W'][-1] + br['truth.tx_b'][-1])
        br['truth.tx_hasElectron'][0] = len(e_pass)
        br['truth.tx_hasMuon'][0] = len(mu_pass)
        br['truth.tx_hasB'][0] = int(b2.auxdata("PartonTruthLabelID") == 5)
        br['truth.ttx'].push_back(br['truth.t'][-1] + br['truth.tx'][-1])
        br['truth.chi2'][0] = min_chi2_b1b2j1j2[0]
        
        if min_chi2_b1b2j1j2[0] >= 0.9:
            return self.CHI2

        return self.ALLPASSED

    def clear(self):
        for branchname, address in self.branches.iteritems():
            if ("_has" in branchname) or ("chi2" in branchname):
                address[0] = 0
                continue
            address.clear()